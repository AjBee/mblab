import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import MainScreen from './src/screen/main'
import Student1Screen from './src/screen/student1'
import Student2Screen from './src/screen/student2'
import Student3Screen from './src/screen/student3'

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Main" component={MainScreen} />
        <Stack.Screen name="Student1" component={Student1Screen} />
        <Stack.Screen name="Student2" component={Student2Screen} />
        <Stack.Screen name="Student3" component={Student3Screen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;