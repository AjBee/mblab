import * as React from 'react';
import { View, Text } from 'react-native';

function Student1Screen() {
  return (
    <View style={{ flex: 1}}>
      <Text>Student1 Screen</Text>
    </View>
  );
}

export default Student1Screen;