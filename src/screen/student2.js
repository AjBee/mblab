import * as React from 'react';
import { View, Text } from 'react-native';

function Student2Screen() {
  return (
    <View style={{ flex: 1}}>
      <Text>Student1 Screen</Text>
    </View>
  );
}

export default Student2Screen;