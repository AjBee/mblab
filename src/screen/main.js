import * as React from 'react';
import { View, Text ,Button} from 'react-native';

function MainScreen({ navigation }) {
  return (
    <View style={{ flex: 1}}>
      <Text>Mian Screen</Text>
      <Button
        title="Student1"
        onPress={() => navigation.navigate('Student1')}
      />

     <Button
        title="Student2"
        onPress={() => navigation.navigate('Student2')}
      />

    <Button
        title="Student3"
        onPress={() => navigation.navigate('Student3')}
      />

    </View>
  );
}

export default MainScreen;